# qgis-plugin-distribution

> A Hello World QGIS plugin, with CI and GitLab Pages providing a way to distribute to end users.

This project demonstrates how GitLab Pages can be used as a repository for QGIS
plugins.
It builds and deploys a modified version of the _Where Am I?_ plugin example
described in [The PyQGIS Programmer's Guide](https://locatepress.com/ppg3).

QGIS Plugins are a very convenient way to write and distribute software for
spatial and database work because:

+ Python is a relatively easy-to-learn language for creating software
+ The Qt5 framework creates nice graphic interfaces
+ All the Qt5 and Python dependencies are installed as part of QGIS
+ QGIS Plugins can easily be installed by anyone with internet access
+ It is easy to create a plugin repository to host your own plugins

The last two points are illustrated by this project.


## How it works

The [.gitlab-ci.yml](.gitlab-ci.yml) file contains setup for a Continuous
Integration (CI) pipeline.  Each time new commits are
pushed to the repository, the pipeline will:

+ Use [pb_tool](https://pypi.org/project/pb-tool/) to create a zipped archive
  of the plugin files.
+ Use the script at [bin/write_plugins_xml.py](bin/write_plugins_xml.py) to
  create a QGIS plugin repository metadata file.
+ Copy both files to be served by GitLab Pages.

[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) is a feature of GitLab that serves a folder of files to the web.
These files are derived from the project and placed in the folder by the CI
pipeline.
A QGIS Plugin repository is just a collection of zip files and an XML file that describes what they are, so GitLab Pages are ideal for this task.

## Installing the plugin

1. Enable the repository in QGIS via _Plugins > Manage and Install Plugins > Settings > Plugin Repositories > Add_.
2. Set the _Name_ to "Hello World"
3. Set the URL to
   `https://volcan01010.gitlab.io/qgis-plugin-distribution/plugins.xml`
4. Press _OK_
5. Search for and enable Hello World in _All_ plugins

> Plugins from the official QGIS repository are checked for malicious code before they are published, whereas plugins from private repositories have not.
> The usual warnings about download and executing code from the internet apply.
> The `__init__` method of an plugin is called automatically when QGIS starts up, so make sure you trust the source.

## Using the plugin

The plugin is installed to _Plugins > Hello World_ in the menu bar.
Click the _Hello World_ to activate the plugin.

When you click on an active map canvas, a dialog window will pop up with the
message "Hello World!" and the coordinates of the point where you clicked.

That's it - have fun!
