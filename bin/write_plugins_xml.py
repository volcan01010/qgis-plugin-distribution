"""Script to prepare QGIS repository plugins.xml file from plugin metadata."""
import argparse
from configparser import ConfigParser
from datetime import date
import os
from textwrap import dedent

TEMPLATE = dedent("""
    <?xml version = '1.0' encoding = 'UTF-8'?>
    <plugins>
      <pyqgis_plugin name='{name}' version='{version}'>
        <description>{description}</description>
        <version>{version}</version>
        <qgis_minimum_version>3.0</qgis_minimum_version>
        <homepage>{homepage}</homepage>
        <file_name>{file_name}</file_name>
        <author_name>{author}</author_name>
        <download_url>{download_url}</download_url>
        <uploaded_by>{uploaded_by}</uploaded_by>
        <create_date>{create_date}</create_date>
        <update_date>{update_date}</update_date>
      </pyqgis_plugin>
    </plugins>
    """).strip()

REPOSITORY_ROOT = "https://volcan01010.gitlab.io/qgis-plugin-distribution"
ZIPFILE_NAME = "hello_world.zip"
CREATE_DATE = "2020-05-08"


def write_plugins_xml(metadata_file):
    """Write plugins.xml from metadata.txt."""
    metadata = ConfigParser()
    metadata.read(metadata_file)
    metadata = metadata['general']

    with open('plugins.xml', 'wt') as outfile:
        outfile.write(TEMPLATE.format(
            file_name=ZIPFILE_NAME,
            download_url=REPOSITORY_ROOT + '/' + ZIPFILE_NAME,
            uploaded_by=os.getenv('USER'),
            create_date=CREATE_DATE,
            update_date=date.today().isoformat(),
            **metadata
        ))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Create plugins.xml file from metadata.txt")
    parser.add_argument("metadata_file", help="Location of metadata.txt",
                        type=str)
    args = parser.parse_args()

    write_plugins_xml(args.metadata_file)
